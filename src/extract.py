#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
script to extract bibliographical references
from plain text found at: ../data/verlagsbibliographie.txt

Creator:          D. Herre
GitLab:      dikon/vag-bib

Created:        2019-07-08
Last Modified:  2020-07-20
"""

import re
import json

# regular expression to find years
re_year = re.compile("^\\d+$")

# regular expression to find entries
re_entry = re.compile("^\\d+[a-z]?\\ \\ +\\S+|^\\d+-\\d+\\ +\\S+")

# regular expression to find start of entry
re_start = re.compile("^\\d+[a-z]?\\s+|\\d+-\\d+\\s+")

# regular expression to find range of entries
re_parts = re.compile("^\\d+-\\d+\\s+")

# regular expression to find parts
re_part = re.compile("(Teil|Band|Slg\\.|Bd\\.|Abt\\.|Stck\\.|Forts\\." +
                     "|\\d+\\.\\sStück:|^Sectio|^(\\S+\\s)?Anhang|^Hauptband" +
                     "|^Fortsetzung|^\\d?\\.?\\s?Sammlung|^Hauptbd\\.|^\\d+\\.\\sLfg\\.|^Heft" +
                     "|\\d+\\.\\sHälfte)(?!\\s\\d+\\]$|\\s\\d;\\sNeue\\sveränd\\.\\sAusg\\.\\]$|\\s\\d\\+\\d\\]$)")

# regular expression to find parts print information
re_parts_print = re.compile("(Teile|\\d\\sBände|Abteilungen|Bde|Sammlungen|" +
                            "(?<!St\\.\\s\\d-\\d\\d\\d.\\sHalle:\\s)Gebauer(\\s\\[u\\.\\sa\\.\\])?,?\\s\\d?\\d\\d\\d\\d-\\d\\d\\d\\d)")

# regular expression to find dates of parts
re_parts_dates = re.compile("\\d\\.\\d+\\s-\\s\\d+\\.\\d+\\s=\\sStück\\s\\d+-\\d+" +
                            "|\\d\\.\\d+\\s-\\s\\d+\\.\\d+\\s=\\sSt\\.\\s\\d+-\\d+" +
                            "|\\d\\.\\d+\\s-\\s\\d+\\.\\d+\\s=\\sNr\\.\\s\\d+-\\d+")

# regular expression to find print information
re_print = re.compile("(Halle: Gebauer|Gebauer,? \\d+)")

# regular expression to find description
re_desc = re.compile("(^\\[?\\d\\d?\\]?\\sBl\\.|^\\d+,|^\\d+\\sS\\.|^[A-Z]+\\sS\\." +
                     "|^\\d\\.,\\sverm\\.|^\\d\\.\\sAufl\\.:|^Neuausg|^\\d\\.,\\sverb\\." +
                     "|^\\[[A-Z]+\\],|^Neue,\\sverm\\.\\sAufl\\.|^[A-Z]+,\\s\\d+\\sS\\." +
                     "|^[A-Z]+,\\s\\d+,)(?!\\.,\\sTeil)")


def read_raw(path='./data/verlagsbibliographie.txt'):
    """
    read the plain text file
    """
    lines = []
    with open(path, mode='r', encoding='utf-8-sig') as f:
        # collect lines without neither line breaks nor non-breaking spaces
        lines = [line.strip().replace('\xa0', ' ') for line in f]
    return lines


def write_json(path, data):
    """
    write given data to json file at path
    """
    print("write to", path)
    with open(path, 'w', encoding="utf-8") as f:
        s = json.dumps(data, indent=2, ensure_ascii=False)
        f.write(s)
    return path


def year_lines(lines):
    """
    detect years and collect their lines
    """
    years = {}
    year_match = -1
    for line in lines:
        # skip line if empty
        if not line:
            continue
        # start of annual sequence
        if re_year.match(line):
            year_match = line
            years[int(year_match)] = []
            continue
        # add content to current years data
        years[int(year_match)].append(line)
    return years


def entry_years(years, output=False):
    """
    detect and collect entries of every year
    """
    y = list(years.keys())
    y.sort()
    total = 0
    entries = {}
    for year in y:
        contents = years[year]
        reference = []
        entries[year] = []
        for line in contents:
            # start of new reference
            if re_entry.match(line):
                # add previous reference if existent
                if reference:
                    entries[year].append(reference)
                    total += 1
                reference = [line]
            # still the same reference
            else:
                reference.append(line)
        # add last reference of year
        if reference:
            entries[year].append(reference)
            total += 1
    print("found", total, "entries!")
    if output:
        write_count_total(total=total)
        write_count_years(entries=entries)
        write_count_refs(entries=entries)
    return entries


def length_entries(entries, output=False):
    """
    count lines ("length") per entry
    """
    len_entries = {}
    for year in entries:
        for entry in entries[year]:
            len_entry = len(entry)
            if len_entry in len_entries:
                len_entries[len_entry].append(entry)
            else:
                len_entries[len_entry] = [entry]
    if output:
        write_count_lines(len_entries=len_entries)
        write_lines_x_y(len_entries=len_entries)
    return len_entries


def structure_entries(entries):
    """
    detect author, title, publication info, ... of every entry
    """
    container = []
    for year in entries:
        for entry in entries[year]:
            start = re_start.match(entry[0])
            counter = start.group().strip()
            creator_title = start.string.replace(start.group(), "")
            creator = creator_title.split(":")[0].strip()
            title = "".join(creator_title.split(":")[1:]).strip()
            structure = {'year': year,
                         'counter': counter,
                         'creator': creator,
                         'title': title}
            if len(entry) > 1:
                if not re_parts_print.search(entry[1]):
                    if 'entspr.' not in entry[1]:
                        if not re_desc.search(entry[1]):
                            structure['print'] = entry[1]
                        else:
                            # 1806-5
                            structure['description'] = entry[1]
                    else:
                        # 1787-6
                        structure['misc'] = [entry[1]]
                else:
                    structure['parts-print'] = entry[1]
            if len(entry) == 3:
                if not re_part.search(entry[2]):
                    if 'Diss.]' not in entry[2]:
                        structure['description'] = entry[2]
                    else:
                        # 1806-5
                        structure['misc'] = [entry[2]]
                else:
                    structure['parts'] = [entry[2]]
            if len(entry) > 3:
                for e in entry[2:]:
                    if re_desc.search(e):
                        structure['description'] = e
                    elif re_parts_print.search(e):
                        structure['parts-print'] = e
                    elif re_parts_dates.search(e):
                        structure['parts-print-dates'] = e
                    elif re_part.search(e):
                        if 'parts' in structure:
                            structure['parts'].append(e)
                        else:
                            structure['parts'] = [e]
                    elif re_print.search(e):
                        if 'print' not in structure:
                            structure['print'] = e
                        else:
                            structure['print-extra'] = e
                    else:
                        if 'misc' in structure:
                            structure['misc'].append(e)
                        else:
                            structure['misc'] = [e]
            container.append(structure)
    return container


####################
# ~~ VALIDATION ~~ #
####################


def check_extraction(entries):
    lines = read_raw()
    print("plain text file consists of", len(lines), "lines")
    content = [line for line in lines if line and re.sub("^\\d\\d\\d\\d$", "", line)]
    print("found", len(content), "lines with content!")
    num = 0
    for entry in entries:
        for elem in entry:
            if elem == "year":
                continue
            elif elem == "counter":
                continue
            elif elem == "creator":
                continue
            elif elem == 'parts':
                num += len(entry[elem])
            elif elem == 'misc':
                num += len(entry[elem])
            else:
                num += 1
    print("extracted elements from", num, "lines!")
    if num == len(content):
        print("everything is fine!")
    else:
        print("something is missing...")


###############
# ~~ UTILS ~~ #
###############

def write_list(filepath, listcontent, title='LIST'):
    with open(filepath, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE"+'\n\n')
        f.write(title+'\n\n')
        for linecontent in listcontent:
            f.write(linecontent + '\n')


def write_list_of_list(filepath, listoflists, title='LIST OF LIST', extra=True):
    with open(filepath, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE"+'\n\n')
        f.write(title+'\n\n')
        for listcontent in listoflists:
            for linecontent in listcontent:
                f.write(linecontent + '\n')
            if extra:
                f.write('\n')

########################
# ~~ EXTRACTION LOG ~~ #
########################


def write_count_total(path='./src/output/count_total.txt', total=0):
    """
    save total number of entries to output/count_total.txt
    """
    with open(path, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nFOUND " + str(total) +
                " ENTRIES!")


def write_count_years(path='./src/output/count_years.txt', entries={}, tab='    '):
    """
    save number of entries per year to output/count_years.txt
    """
    with open(path, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nYEAR:   COUNT\n\n")
        for year in entries:
            f.write(str(year) + ":" + tab + str(len(entries[year])) + "\n")


def write_count_refs(path='./src/output/count_refs.txt', entries={}, tab='    '):
    """
    save total number of references to output/count_references.txt
    (volumes from multi volumes count as 1) 
    """
    total = 0
    with open(path, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\n")
        for year in entries:
            entries_year = entries[year]
            for entry in entries_year:
                if re_parts.match(entry[0]):
                    parts = re_parts.match(entry[0]).group().strip().split("-")
                    num_parts = int(parts[1])-int(parts[0])+1
                    total += num_parts
                else:
                    total += 1
        f.write("FOUND " + str(total) + " REFERENCES!")
    print("found", total, "references!")


def write_count_lines(path='./src/output/count_lines.txt', len_entries={}, tab='    '):
    """
    save count of entries with lines of length x to output/count_lines.txt
    """
    with open(path, mode='w+', encoding='utf-8') as f:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nLINES PER ENTRY :    COUNT\n\n")
        le = list(len_entries.keys())
        le.sort()
        for len_entry in le:
            f.write(tab + str(len_entry) + tab + tab + "   :" + tab + " " +
                    str(len(len_entries[len_entry])) + "\n")


def write_lines_x_y(out_dir='./src/output/', len_entries={}, tab='    '):
    """
    save content of line x from entries with length y to output/lines_x.txt
    """
    with open(out_dir + 'lines_first.txt', mode='w+', encoding='utf-8') as f,\
        open(out_dir + 'lines_secnd.txt', mode='w+', encoding='utf-8') as g,\
        open(out_dir + 'lines_third.txt', mode='w+', encoding='utf-8') as h,\
        open(out_dir + 'lines_x-y.txt', mode='w+', encoding='utf-8') as i:
        f.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nFIRST LINES\n\n")
        g.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nSECOND LINES\n\n")
        h.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nTHIRD LINES\n\n")
        i.write("GEBAUER VERLAGSBIBLIOGRAPHIE\n\nLINES > THREE\n\n")
        le = list(len_entries.keys())
        le.sort()
        for len_entry in le:
            if len_entry > 3:
                i.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                for entry in len_entries[len_entry]:
                    for j in range(3, len_entry):
                        # if j < len(entry):
                        i.write(str(j+1) + tab + entry[j] + "\n")
                    if len_entry > 4:
                        i.write("\n")
                i.write("\n")
            if len_entry > 2:
                f.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                g.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                h.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                for entry in len_entries[len_entry]:
                    f.write(entry[0] + "\n")
                    g.write(entry[1] + "\n")
                    h.write(entry[2] + "\n")
                f.write("\n")
                g.write("\n")
                h.write("\n")
            elif len_entry > 1:
                f.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                g.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                for entry in len_entries[len_entry]:
                    f.write(entry[0] + "\n")
                    g.write(entry[1] + "\n")
                f.write("\n")
                g.write("\n")
            else:
                f.write("\nENTRIES WITH LENGTH  " + str(len_entry) + " :\n\n")
                for entry in len_entries[len_entry]:
                    f.write(entry[0] + "\n")
                f.write("\n")


def write_total_elements(entries):
    """
    save elements extracted from entries to seperate files
    """
    years = [str(entry['year']) for entry in entries]
    write_list('./src/output/total_years.txt', years, title='TOTAL YEARS')
    counter = [entry['counter'] for entry in entries]
    write_list('./src/output/total_counters.txt', counter, title='TOTAL COUNTERS')
    creators = [entry['creator'] for entry in entries]
    write_list('./src/output/total_creators.txt', creators, title='TOTAL CREATORS')
    titles = [entry['title'] for entry in entries]
    write_list('./src/output/total_titles.txt', titles, title='TOTAL TITLES')
    print = [entry['print'] for entry in entries if 'print' in entry]
    write_list('./src/output/total_prints.txt', print, title='TOTAL PRINTS')
    print_extras = [entry['print-extra'] for entry in entries if 'print-extra' in entry]
    write_list('./src/output/total_print-extras.txt', print_extras, title='TOTAL PRINT EXTRAS')
    description = [entry['description'] for entry in entries if 'description' in entry]
    write_list('./src/output/total_descriptions.txt', description, title='TOTAL DESCRIPTIONS')
    miscs = [entry['misc'] for entry in entries if 'misc' in entry]
    write_list_of_list('./src/output/total_misc.txt', miscs, title='TOTAL MISCELLANEOUS', extra=False)
    parts = [entry['parts'] for entry in entries if 'parts' in entry]
    write_list_of_list('./src/output/total_parts.txt', parts, title='TOTAL PARTS (DESCRIPTION)')
    parts_print = [entry['parts-print'] for entry in entries if 'parts-print' in entry]
    write_list('./src/output/total_parts-print.txt', parts_print, title='TOTAL PARTS (PRINT)')
    parts_print_dates = [entry['parts-print-dates'] for entry in entries if 'parts-print-dates' in entry]
    write_list('./src/output/total_parts-print-dates.txt', parts_print_dates, title='TOTAL PARTS (DATES)')


def write_creator_roles(entries):
    """
    save creators (authors, editors, ... ) extracted from entries to seperate files
    """
    creators = [entry['creator'] for entry in entries]
    creators_roles = [creator for creator in creators if "(" in creator]
    write_list('./src/output/total_creators_roles.txt', creators_roles, title="CREATOR WITH ROLE")
    creators_editors = [creator for creator in creators_roles if "(Hg.)" in creator or "(Hg)" in creator]
    write_list('./src/output/total_creators_roles_editors.txt', creators_editors,\
               title="ROLE OF CREATOR: EDITOR")
    creator_translators = [creator for creator in creators_roles if "(Übers.)" in creator]
    write_list('./src/output/total_creators_roles_translators.txt', creator_translators,\
               title="ROLE OF CREATOR: TRANSLATOR")
    creator_contributors = [creator for creator in creators_roles if "(Beitr.)" in creator]
    write_list('./src/output/total_creators_roles_contributors.txt', creator_contributors,\
               title="ROLE OF CREATOR: CONTRIBUTOR")
    other = [creator for creator in creators_roles if creator not in creators_editors and\
             creator not in creator_translators and creator not in creator_contributors]
    write_list('./src/output/total_creators_roles_special.txt', other,\
               title="ROLE OF CREATOR: SPECIAL")
    creators_authors = [creator for creator in creators if creator not in creators_roles]
    write_list('./src/output/total_creators_roles_authors.txt', creators_authors,\
               title="ROLE OF CREATOR: AUTHOR")
    creators_authors_special = [creator for creator in creators_authors if "," not in creator]
    write_list('./src/output/total_creators_roles_authors_special.txt', creators_authors_special,\
               title="ROLE OF CREATOR: AUTHOR (SPECIAL FORMAT)")
    creators_authors_normal = [creator for creator in creators_authors if creator not in creators_authors_special]
    write_list('./src/output/total_creators_roles_authors_normal.txt', creators_authors_normal,\
               title="ROLE OF CREATOR: AUTHOR (NORMAL FORMAT)")


#################
# ~~ WRAPPER ~~ #
#################


def extract_entries(output=True):
    lines = read_raw()
    years = year_lines(lines)
    entries = entry_years(years, output=output)
    if output:
        len_entries = length_entries(entries, output=output)
    entries = structure_entries(entries)
    if output:
        write_json('./data/verlagsbibliographie.json', entries)
        write_total_elements(entries)
        write_creator_roles(entries)
    return entries

##############
# ~~ MAIN ~~ #
##############


if __name__ == "__main__":
    extract_entries()
