GEBAUER VERLAGSBIBLIOGRAPHIE

TOTAL PARTS (PRINT)

3 Teile. Halle, druckts und verlegts Joh. Justinus Gebauer, 1737
2 Bde. Halle, gedruckt und verlegt von Joh. Justinus Gebauer, 1738 [21738]
2 Teile. Halle im Magdeburgischen / Gedruckt und verlegt von Johann Justinus Gebauer, 1739
24 Teile. Halle: Gebauer 1740-1753
24 Teile. Halle: Gebauer 1740-1753
24 Teile. Halle: Gebauer 1740-1753
4 Teile. Halle: Gebauer 21743-1762
24 Teile. Halle: Gebauer 1740-1753
66 Teile. Halle: Gebauer 1744-1814
Halle: Gebauer 21744-1753
24 Teile. Halle: Gebauer 1740-1753
66 Teile. Halle: Gebauer 1744-1814
24 Teile. Halle: Gebauer 1740-1753
66 Teile. Halle: Gebauer 1744-1814
24 Teile. Halle: Gebauer 1740-1753
2 Teile. Halle: Gebauer 1747, 1753
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 1747-1765
4 Teile. Halle: Gebauer 1747-1748
24 Teile. Halle: Gebauer 1740-1753
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 1747-1765
Halle: Gebauer 1748-1751
4 Teile. Halle: Gebauer 21743-1762
Halle: Gebauer 1748-1750
4 Sammlungen. Halle: Gebauer 1748-1752
4 Teile. Halle: Gebauer 1747-1748
66 Teile. Halle: Gebauer 1744-1814
Halle: Gebauer 1748-1751
4 Sammlungen. Halle: Gebauer 1748-1752
24 Teile. Halle: Gebauer 1740-1753
4 Teile. Halle: Gebauer 1750-1752
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 1747-1765
Halle: Gebauer 1748-1751
4 Sammlungen. Halle: Gebauer 1748-1752
24 Teile. Halle: Gebauer 1740-1753
2 Bde. Halae Magdeburgicae, Typis Ioannis Iustini Gebauer, [1750]
4 Teile. Halle: Gebauer 1750-1752
Halle: Gebauer 1748-1751
66 Teile. Halle: Gebauer 1744-1814
Halle: Gebauer 1751-1756
2 Teile. Halle: Gebauer 1751-1754
4 Teile. Halle: Gebauer 1750-1752
2 Teile. Halle, bey Johann Justinus Gebauer, 1752
66 Teile. Halle: Gebauer 1744-1814
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
2 Teile. Halle, bey Johann Justinus Gebauer, 1752
5 Teile. Halle: Gebauer 1751-1755
7 Teile. Halle: Gebauer 1752-1755
2 Teile. Halle: Gebauer 1752-1756
2 Teile. Halle, bey Joh. Justinus Gebauer. 1752
Halle: Gebauer 1751-1756
2 Teile. Halle: Gebauer 1752-1753
2 Teile. Halle: Gebauer 1751-1754
2 Teile. Halle: Gebauer 1747, 1753
66 Teile. Halle: Gebauer 1744-1814
4 Teile. Halle: Gebauer 21743-1762
2 Sammlungen. Halle: Gebauer 1753-1755
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
5 Teile. Halle: Gebauer 1751-1755
Halle: Gebauer 1751-1756
6 Bände. Halle: Gebauer 1753-1755
2 Teile. Halle: Gebauer 1752-1753
24 Teile. Halle: Gebauer 1740-1753
2 Teile. Halle: Gebauer 1754-1755
66 Teile. Halle: Gebauer 1744-1814
10 Teile. Halle: Gebauer 1754-1770
2 Teile. Halle: Gebauer 1754-1755
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
7 Teile, 2 Bände. Halle: Gebauer 1752-1755
13 Bände. Halle: Gebauer 1754-1772
Halle: Gebauer 1751-1756
6 Bände. Halle: Gebauer 1753-1755
2 Teile. Halae, Litteris Gebauerianis. [1754]
2 Teile. Halae, Litteris Gebauerianis. [1754]
2 Teile. Halle: Gebauer 1751-1754
66 Teile. Halle: Gebauer 1744-1814
2 Sammlungen. Halle: Gebauer 1753-1755
2 Teile. Halle: Gebauer 1754-1755
2 Teile. Halle, bey Johann Justinus Gebauer, 1755 [11752]
2 Teile. Halle: Gebauer 1754-1755
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
10 Teile. Halle: Gebauer 1754-1770
5 Teile. Halle: Gebauer 1751-1755
8 Teile. Halle: Gebauer 1752-1755
13 Bände. Halle: Gebauer 1754-1772
4 Teile. Halle: Gebauer 1755-1759
Halle: Gebauer 1751-1756
6 Bände. Halle: Gebauer 1753-1755
6 Teile. Halle: Gebauer 1747-1765
66 Teile. Halle: Gebauer 1744-1814
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
2 Teile. Halle: Gebauer 1752-1756
13 Bände. Halle: Gebauer 1754-1772
Halle: Gebauer 1751-1756
4 Teile. Halle: Gebauer 1755-1759
9 Teile. Halle: Gebauer 1756-1764
5 Bände. Halle: Gebauer 1756-1758
2 Teile. Halle: Gebauer 11754-1755, 21757
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
10 Teile. Halle: Gebauer 1754-1770
13 Bände. Halle: Gebauer 1754-1772
4 Teile. Halle: Gebauer 1755-1759
9 Teile. Halle: Gebauer 1756-1764
5 Bände. Halle: Gebauer 1756-1758
66 Teile. Halle: Gebauer 1744-1814
Stücke 1-66, 12 Bände. Halle: Gebauer 1752-1758
4 Teile. Halle: Gebauer 1758-1760
13 Bände. Halle: Gebauer 1754-1772
9 Teile. Halle: Gebauer 1756-1764
5 Bände. Halle: Gebauer 1756-1758
2 Teile. Halle: Gebauer 11747-1748, 21758
3 Bde. Halle: Gebauer 1759-1760
2 Teile. Halle, bey Johann Justinus Gebauer, 1759
4 Teile. Halle: Gebauer 1758-1760
4 Teile. Halle: Gebauer 1755-1759
9 Teile. Halle: Gebauer 1756-1764
2 Teile. Halle, bey Johann Immanuel Gebauer, 21759-1760
66 Teile. Halle: Gebauer 1744-1814
10 Teile. Halle: Gebauer 1754-1770
3 Bde. Halle: Gebauer 1759-1760
4 Teile. Halle: Gebauer 1758-1760
13 Bände. Halle: Gebauer 1754-1772
9 Teile. Halle: Gebauer 1756-1764
2 Teile. Halle, bey Johann Immanuel Gebauer, 1759-1760
66 Teile. Halle: Gebauer 1744-1814
Halle: Gebauer 1761-1767
9 Teile. Halle: Gebauer 1756-1764
6 Teile. Halle: Johann Immanuel Gebauer & August Leberecht Stettin, 1761-1762; Halle: Johann Immanuel Gebauer 1763-1765
10 Teile. Halle: Gebauer 1754-1770
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 1747-1765
3 Bände. Halle: Gebauer 1762-1764
13 Bände. Halle: Gebauer 1754-1772
2 Bände. Halle im Magdeburgischen, in der Gebauer- und Stettinischen Buchhandlung. 1762 [Johann Immanuel Gebauer & August Leberecht Stettin]
Halle: Gebauer, 1757-1762
Halle: Gebauer 1761-1767
9 Teile. Halle: Gebauer 1756-1764
Halle: Gebauer [u. a.] 1757-1764
6 Teile. Halle: Johann Immanuel Gebauer & August Leberecht Stettin, 1761-1762; Halle: Johann Immanuel Gebauer 1763-1765
4 Teile. Halle: Gebauer 1748-1762
10 Teile. Halle: Gebauer 1754-1770
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: Gebauer 1762-1764
8 Teile. Halle: J. I. Gebauer 1763-1768
2 Teile. Halle, bey Johann Immanuel Gebauer. 1763; à Halle / chez Jean Emanuel Gebauer. M DCC LXIII
Halle: Gebauer, 1763-1768
Halle: Gebauer 1761-1767
9 Teile. Halle: Gebauer 1756-1764
6 Teile. Halle: Johann Immanuel Gebauer & August Leberecht Stettin, 1761-1762; Halle: Johann Immanuel Gebauer 1763-1765
66 Teile. Halle: Gebauer 1744-1814
3 Bände. Halle: Gebauer 1762-1764
8 Teile. Halle: J. I. Gebauer 1763-1768
Halle: Gebauer 1761-1767
Halle: Gebauer, 1763-1768
9 Teile. Halle: Gebauer 1756-1764
6 Teile. Halle: Johann Immanuel Gebauer & August Leberecht Stettin, 1761-1762; Halle: Johann Immanuel Gebauer 1763-1765
66 Teile. Halle: Gebauer 1744-1814
3 Bände. Halle: Gebauer 11759-1760, 21764-1765
3 Bände. Halle: Gebauer 1762-1764
2 Teile. Halle: Gebauer 1762-1764
2 Teile. Halle: 1765-1767
8 Teile. Halle: J. I. Gebauer 1763-1768
Halle: Gebauer 1761-1767
Halle: Gebauer, 1763-1768
6 Teile. Halle: Johann Immanuel Gebauer & Stettin, 1761-1762; Halle: Johann Immanuel Gebauer 1763-1765
6 Teile. Halle: Gebauer 1747-1765
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: 1765-1767
8 Teile. Halle: J. I. Gebauer 1763-1768
Halle: Gebauer 1761-1767
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: 1765-1767
6 Stücke. Halle: Gebauer 1767-1770
Halle: Gebauer 1767-1772
16 Bände. Halle: Gebauer 1767-1771
12 Bde. Halle: Gebauer 1767-1773
6 Bände. Halle: Gebauer 1767-1771
Halle: Gebauer 1761-1767
Halle: Gebauer 1767-1772
6 Stücke. Halle: Gebauer 1767-1770
8 Teile. Halle: J. I. Gebauer 1763-1768
16 Bände. Halle: Gebauer 1767-1771
66 Teile. Halle: Gebauer 1744-1814
12 Bde. Halle: Gebauer 1767-1773
6 Bände. Halle: Gebauer 1767-1771
Halle: Gebauer 1768-1769
Halle: Gebauer 1767-1772
6 Stücke. Halle: Gebauer 1767-1770
Halle: Gebauer 1754-1772
16 Bände. Halle: Gebauer 1767-1771
12 Bde. Halle: Gebauer 1767-1773
6 Bände. Halle: Gebauer 1767-1771
10 Teile. Halle: Gebauer 1754-1770
6 Stücke. Halle: Gebauer 1767-1770
10 Bände. Halle: Gebauer 1767-1772
16 Bände. Halle: Gebauer 1767-1771
66 Teile. Halle: Gebauer 1744-1814
3 Teile. Halle: Gebauer 1770-1773
12 Bde. Halle: Gebauer 1767-1773
6 Bände. Halle: Gebauer 1767-1771
6 Teile. Halle: Gebauer 1770-1774
10 Teile. Halle: Gebauer 1754-1770
66 Teile. Halle: Gebauer 1744-1814
10 Bde. Halle: Gebauer 1767-1772
16 Bde. Halle: Gebauer 1767-1771
12 Bde. Halle: Gebauer 1767-1773
6 Bände. Halle: Gebauer 1767-1771
6 Teile. Halle: Gebauer 1770-1774
4 Teile. Halle: Gebauer 1771-1773
66 Teile. Halle: Gebauer 1744-1814
6 Bde. Halle: Gebauer 1772-1784
7 Bde. Halle: Gebauer 1772-1781
2 Teile. Halle: Gebauer 1772-1773 [1. Aufl. Halle: Renger 1769]
10 Bde. Halle: Gebauer 1767-1772
13 Bde. Halle: Gebauer 1754-1772
12 Bde. Halle: Gebauer 1767-1773
66 Teile. Halle: Gebauer 1744-1814
4 Bde. Halle: Gebauer 1772-1776
6 Teile. Halle: Gebauer 1770-1774
4 Teile. Halle: Gebauer 1771-1773
6 Bde. Halle: Gebauer 1772-1784
7 Bde. Halle: Gebauer 1772-1781
2 Teile. Halle: Gebauer 1772-1773
4 Teile / 204 Stück. Halle: Gebauer 1773-1774
12 Bde. Halle: Gebauer 1767-1773
3 Teile. Halle: Gebauer 1770-1773
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 1770-1774
4 Bde. Halle: Gebauer 1772-1776
4 Teile. Halle: Gebauer 1771-1773
4 Bde. Halle: Gebauer 1773-1780
6 Bde. Halle: Gebauer 1772-1784
7 Bde. Halle: Gebauer 1772-1781
4 Teile. / 204 Stück. Halle: Gebauer 1773-1774
27 Bde. Halle: Gebauer 1767-1790
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
66 Teile. Halle: Gebauer 1744-1814
4 Teile. Halle: Gebauer 1774-1777
6 Teile. Halle: Gebauer 1770-1774
4 Bde. Halle: Gebauer 1772-1776
5 Teile. Halle: Gebauer 1774-1780
4 Bde. Halle: Gebauer 1773-1780
30 Stücke. Halle: Gebauer 1774-1804
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
7 Teile. Halle: Gebauer 1775-1779
4 Teile. Halle: Gebauer 1774-1777
5 Teile. Halle: Gebauer 1775-1782
3 Teile. Halle: Gebauer 1775-1778
5 Teile. Halle: Gebauer 1774-1780
27 Bde. Halle: Gebauer 1767-1790
30 Stücke. Halle: Gebauer 1774-1804
4 Bde. Halle: Gebauer 1772-1776
6 Bde. Halle: Gebauer 1772-1784
7 Bde. Halle: Gebauer 1772-1781
6 Sammlungen. Halle: Gebauer 1776-1790
3 Bde. Halle: Gebauer 1776-1785
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1776-1777
66 Teile. Halle: Gebauer 1744-1814
7 Teile. Halle: Gebauer 1775-1779
4 Teile. Halle: Gebauer 1774-1777
5 Teile. Halle: Gebauer 1775-1782
3 Teile. Halle: Gebauer 1775-1778
4 Bde. Halle: Gebauer 1772-1776
2 Teile. Halle: Gebauer 1776-1777
5 Teile. Halle: Gebauer 1774-1780
3 Teile. Halle: Gebauer 1776-1780
30 Stücke. Halle: Gebauer 1774-1804
6 Bde. Halle: Gebauer 1777-1781
2 Bde. Halle: Gebauer 1777-1778
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1776-1777
27 Bde. Halle: Gebauer 1767-1790
7 Teile. Halle: Gebauer 1775-1779
4 Teile. Halle: Gebauer 1774-1777
5 Teile. Halle: Gebauer 11775-1782, 21777-1795, 31778-1795, 41780-1795, 51794-1795, 61830-1831
2 Teile. Halle: Gebauer 1776-1777
3 Teile. Halle: Gebauer 1776-1780
4 Bde. Halle: Gebauer 1773-1780
30 Stücke. Halle: Gebauer 1774-1804
4 Teile. Halle: Gebauer 1777-1781
6 Bde. Halle: Gebauer 1772-1784
7 Bde. Halle: Gebauer 1772-1781
6 Bde. Halle: Gebauer 1777-1781
2 Bde. Halle: Gebauer 1777-1778
6 Sammlungen. Halle: Gebauer 1776-1790
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1778-1779
2 Teile. Halle: Gebauer 1778-1780
66 Teile. Halle: Gebauer 1744-1814
7 Stücke. Halle: Gebauer 1778-1782
2 Teile. Halle: Gebauer 1778-1790
27 Bde. Halle: Gebauer 1767-1790
7 Teile. Halle: Gebauer 1775-1779
5 Teile. Halle: Gebauer 31778-1795 [11775-1782]
2 Bände. Halle: Gebauer 1778-1781
3 Teile. Halle: Gebauer 1775-1778
30 Stücke. Halle: Gebauer 1774-1804
6 Bde. Halle: Gebauer 1777-1781
6 Sammlungen. Halle: Gebauer 1776-1790
3 Bde. Halle: Gebauer 1776-1785
28 Bde. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1778-1779
66 Teile. Halle: Gebauer 1744-1814
7 Stücke. Halle: Gebauer 1778-1782
7 Teile. Halle: Gebauer 1775-1779
27 Bde. Halle: Gebauer 1767-1790
5 Teile. Halle: Gebauer 1775-1782
30 Stücke. Halle: Gebauer 1774-1804
3 Teile. Halle: Gebauer 1779-1781
4 Teile. Halle: Gebauer 1780-1781
2 Teile. Halle: Gebauer 1780-1781
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1778-1780
66 Teile. Halle: Gebauer 1744-1814
7 Stücke. Halle: Gebauer 1778-1782
5 Teile. Halle: Gebauer 41780-1795 [11775-1782]
30 Stücke. Halle: Gebauer 1774-1804
5 Teile. Halle: Gebauer 1774-1780
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1780-1781
3 Teile. Halle: Gebauer 1779-1781
3 Teile. Halle: Gebauer 1776-1780
4 Bände. Halle: Gebauer 1773-1780
4 Teile. Halle: Gebauer 1777-1781
4 Teile. Halle: Gebauer 1780-1781
7 Bde. Halle: Gebauer 1772-1781
6 Bände. Halle: Gebauer 1777-1781
2 Teile. Halle: Gebauer 1780-1781
6 Teile. Halle: Gebauer 1776-1790
2 Teile. Halle: Gebauer 1781-1782
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
66 Teile. Halle: Gebauer 1744-1814
7 Stücke. Halle: Gebauer 1778-1782
2 Teile. Halle: Gebauer 1778-1790
4 Teile. Halle: Gebauer 1781-1782
5 Teile. Halle: Gebauer 31778-1795 [11775-1782]
2 Bände. Halle: Gebauer 1778-1781
30 Stücke. Halle: Gebauer 1774-1804
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1780-1781
3 Teile. Halle: Gebauer 1779-1781
Halle: Gebauer 1781-1783
4 Teile. Halle: Gebauer 1777-1781
7 Bände. Halle: Gebauer 1782-1787
2 Teile. Halle: Gebauer 1781-1782
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
66 Teile. Halle: Gebauer 1744-1814
7 Stücke. Halle: Gebauer 1778-1782
4 Teile. Halle: Gebauer 1781-1782
5 Teile. Halle: Gebauer 1775-1782
30 Stücke. Halle: Gebauer 1774-1804
3 Teile. Halle: Gebauer 1782-1794
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1782-1784
Halle: Gebauer 1781-1783
2 Teile. Halle: Gebauer 1783-1784
4 Stücke. Halle: Gebauer 1783-1785
2 Teile. Halle: Gebauer 1783-1784
7 Bände. Halle: Gebauer 1782-1787
2 Bände. Halle: Gebauer 1783-1786
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Teile. Halle: Gebauer 1778-1790
2 Sammlungen. Halle: Gebauer 1783-1789
2 Bde. Halle: Gebauer 1783-1784
2 Teile. Halle: Gebauer 1783-1784
30 Stücke. Halle: Gebauer 1774-1804
3 Teile. Halle: Gebauer 1782-1794
13 Teile. Halle: Gebauer 1780-1791
3 Bde. Halle: Gebauer 1783-1786
Halle: Gebauer 1781-1783
2 Teile. Halle: Gebauer 1783-1785
66 Teile. Halle: Gebauer 1744-1814
6 Bde. Halle: Gebauer 1772-1784
2 Teile. Halle: Gebauer 1783-1784
4 Stücke. Halle: Gebauer 1783-1785
2 Teile. Halle: Gebauer 1783-1784
7 Bände. Halle: Gebauer 1782-1787
6 Teile. Halle: Gebauer 1776-1790
2 Teile. Halle: Gebauer 1784-1785
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
66 Teile. Halle: Gebauer 1744-1814
2 Bde. Halle: Gebauer 1783-1784
2 Teile. Halle: Gebauer 1783-1784
30 Stücke. Halle: Gebauer 1774-1804
3 Bde. Halle: Gebauer 1783-1786
3 Teile. Halle: Gebauer 1782-1794
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1784-1786
4 Stücke. Halle: Gebauer 1783-1785
7 Bände. Halle: Gebauer 1782-1787
6 Teile. Halle: Gebauer 1776-1790
2 Teile. Halle: Gebauer 1784-1785
3 Bände. Halle: Gebauer 1776-1785
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
4 Bände. Halle: Gebauer 1785-1790
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: Gebauer 1778-1790
5 Teile. Halle: Gebauer 21777-1795 [11775-1782]
66 Teile. Halle: Gebauer 1744-1814
30 Stücke. Halle: Gebauer 1774-1804
2 Bände. Halle: Gebauer 1785-1786
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1783-1785
2 Bände. Halle: Gebauer 1785-1786
3 Teile. Halle: Gebauer 1785-1793
2 Stücke. Halle: Gebauer 1785-1786
7 Bände. Halle: Gebauer 1782-1787
2 Bände. Halle: Gebauer 1783-1786
2 Bände. Halle: Gebauer 1786
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
66 Teile. Halle: Gebauer 1744-1814
2 Bände. Halle: Gebauer 1785-1786
3 Bde. Halle: Gebauer 1783-1786
2 Teile. Halle: Gebauer 1786-1787
2 Teile. Halle: Gebauer 1784-1786
2 Bände. Halle: Gebauer 1785-1786
2 Stücke. Halle: Gebauer 1785-1786
2 Teile. Halle: Gebauer 1787-1788
7 Bände. Halle: Gebauer 1782-1787
66 Teile. Halle: Gebauer 1744-1814
4 Bände. Halle: Gebauer 1785-1790
66 Teile. Halle: Gebauer 1744-1814
27 Bände. Halle: Gebauer 1767-1790
30 Stücke. Halle: Gebauer 1774-1804
2 Teile. Halle: Gebauer 1786-1787
13 Teile. Halle: Gebauer 1780-1791
8 Teile. Halle: Gebauer 1787-1794
3 Teile. Halle: Gebauer 1785-1793
2 Teile. Halle: Gebauer 1787-1788
4 Bde. Halle: Gebauer 1788-1792
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: Gebauer 1788-1795
27 Bände. Halle: Gebauer 1767-1790
30 Stücke. Halle: Gebauer 1774-1804
13 Teile. Halle: Gebauer 1780-1791
8 Teile. Halle: Gebauer 1787-1794
2 Teile. Halle: Gebauer 1789-1791
4 Teile. Halle: Gebauer 1789-1800
4 Bde. Halle: Gebauer 1788-1792
66 Teile. Halle: Gebauer 1744-1814
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
4 Bände. Halle: Gebauer 1785-1790
2 Teile. Halle: Gebauer 1789
2 Teile. Halle: Gebauer 1788-1795
27 Bände. Halle: Gebauer 1767-1790
2 Sammlungen. Halle: Gebauer 1783-1789
2 Bände. Halle: Gebauer 1789-1790
8 Abteilungen. Halle: Gebauer 1789-1794
30 Stücke. Halle: Gebauer 1774-1804
13 Teile. Halle: Gebauer 1780-1791
2 Teile. Halle: Gebauer 1789-1790
8 Teile. Halle: Gebauer 1787-1794
2 Bde. Halle: Gebauer 1789-1790
4 Teile. Halle: Gebauer 1789-1800
4 Bde. Halle: Gebauer 1788-1792
6 Teile. Halle: Gebauer 1776-1790
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
4 Bände. Halle: Gebauer 1785-1790
2 Teile. Halle: Gebauer 1788-1795
27 Bände. Halle: Gebauer 1767-1790
2 Teile. Halle: Gebauer 1778-1790
2 Bände. Halle: Gebauer 1789-1790
8 Abteilungen. Halle: Gebauer 1789-1794
6 Stücke. Halle: Gebauer 1790-1794
2 Teile. Halle: Gebauer 1789-1790
13 Teile. Halle: Gebauer 1780-1791
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
8 Teile. Halle: Gebauer 1787-1794
2 Teile. Halle: Gebauer 1789-1791
4 Bde. Halle: Gebauer 1788-1792
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
2 Teile. Halle: Gebauer 1788-1795
8 Abteilungen. Halle: Gebauer 1789-1794
30 Stücke. Halle: Gebauer 1774-1804
6 Stücke. Halle: Gebauer 1790-1794
2 Bde. Halle: Gebauer 1791
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
2 Bde. Halle: Gebauer 1791-1794
2 Teile. Halle: Gebauer 1791-1792
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
2 Teile. Halle: Gebauer 1788-1795
8 Abteilungen. Halle: Gebauer 1789-1794
30 Stücke. Halle: Gebauer 1774-1804
4 Teile. Halle: Gebauer 1792-1799
6 Stücke. Halle: Gebauer 1790-1794
13 Teile. Halle: Gebauer 1780-1791
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
5 Teile. Halle: Gebauer 1792-1803
2 Bde. Halle: Gebauer 1791-1794
2 Teile. Halle: Gebauer 1791-1792
2 Lieferungen. Halle: Gebauer 1793-1797
66 Teile. Halle: Gebauer 1744-1814
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
2 Teile. Halle: Gebauer 1788-1795
8 Abteilungen. Halle: Gebauer 1789-1794
30 Stücke. Halle: Gebauer 1774-1804
4 Teile. Halle: Gebauer 1792-1799
6 Stücke. Halle: Gebauer 1790-1794
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
5 Teile. Halle: Gebauer 1792-1803
3 Teile. Halle: Gebauer 1793-1795
8 Teile. Halle: Gebauer 1787-1794
3 Teile. Halle: Gebauer 1785-1793
66 Teile. Halle: Gebauer 1744-1814
7 Bände. Halle: Gebauer 1789-1795
5 Teile. Halle: Gebauer 51794-1795 [11775-1782]
8 Abteilungen. Halle: Gebauer 1789-1794
3 Teile. Halle: Gebauer 1782-1794
4 Teile. Halle: Gebauer 1792-1799
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
5 Teile. Halle: Gebauer 1792-1803
3 Teile. Halle: Gebauer 1793-1795
2 Bde. Halle: Gebauer 1791-1794
8 Teile. Halle: Gebauer 1787-1794
66 Teile. Halle: Gebauer 1744-1814
2 Bände. Halle: Gebauer 11786, 21795-1796
7 Bände. Halle: Gebauer 1789-1795
2 Teile. Halle: Gebauer 1788-1795
5 Teile. Halle: Gebauer 11775-1782
13 Teile. Halle: Gebauer 11780-1791
28 Bände. Halle: Gebauer 1774-1804 [Bd. 27 u. 28 Frankfurt/ M.: Macklot, Gebhard]
3 Teile. Halle: Gebauer 1793-1795
8 Teile. Halle: Gebauer 11787-1794
66 Teile. Halle: Gebauer 1744-1814
3 Teile. Halle: Gebauer 1796-1798
66 Teile. Halle: Gebauer 1744-1814
66 Teile. Halle: Gebauer 1744-1814
2 Bände. Halle: Gebauer 11786, 21795-1796
8 Teile. Halle: Gebauer 11787-1794
4 Teile. Halle: Gebauer 1797-1800
2 Lieferungen. Halle: Gebauer 1793-1797
66 Teile. Halle: Gebauer 1744-1814
3 Teile. Halle: Gebauer 1796-1798
66 Teile. Halle: Gebauer 1744-1814
8 Teile. Halle: Gebauer 11787-1794
4 Teile. Halle: Gebauer 1797-1800
66 Teile. Halle: Gebauer 1744-1814
3 Teile. Halle: Gebauer 1796-1798
2 Bde. Halle: Gebauer 1798-1799
3 Teile. Halle: Gebauer 1782-1794
8 Teile. Halle: Gebauer 11787-1794
4 Teile. Halle: Gebauer 1797-1800
2 Bde. Halle: Gebauer 1798-1799
30 Stücke. Halle: Gebauer 1774-1804
13 Teile. Halle: Gebauer 11780-1791
4 Teile. Halle: Gebauer 1792-1799
5 Teile. Halle: Gebauer 1792-1803
2 Teile. Halle: Gebauer 1799-1800
4 Teile. Halle: Gebauer 1797-1800
4 Bde. Halle: Gebauer 1789-1800
2 Teile. Halle: Gebauer 1800-1801
3 Teile. Halle: Gebauer 11782-1794, 21799-1808, 31809-1821
5 Teile. Halle: Gebauer 11792-1799, 21800-1818, 31821-1840
2 Teile. Halle: Gebauer 1799-1800
66 Teile. Halle: Gebauer 1744-1814
2 Teile. Halle: Gebauer 1800-1801
5 Teile. Halle: Gebauer 11782-1821, 21799-1808, 31809-1811
13 Teile. Halle: Gebauer 11780-1792, 2-51794-1807
5 Teile. Halle: Gebauer 11792-1799, 21800-1818, 31821-1840
3 Teile. Halle: Gebauer 1801-1803
8 Bände. Halle: Gebauer 1802-1809
30 Stücke. Halle: Gebauer 1774-1804
8 Bände. Halle: Gebauer 1802-1809
3 Teile. Halle: Gebauer 1801-1803
2 Teile. Halle: Gebauer 1803-1808
66 Teile. Halle: Gebauer 1744-1814
13 Teile. Halle: Gebauer 11780-1792, 2-51794-1807
3 Bde. Halle: Gebauer 1803-1806
5 Teile. Halle: Gebauer 11792-1799, 21800-1818, 31821-1840
3 Teile. Halle: Gebauer 1801-1803
8 Bände. Halle: Gebauer 1802-1809
66 Teile. Halle: Gebauer 1744-1814
30 Stück. Halle: Gebauer 1774-1804
3 Bde. Halle: Gebauer 1803-1806
4 Bde. Halle: Gebauer 1804-1806
8 Bände. Halle: Gebauer 1802-1809
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
66 Teile. Halle: Gebauer 1744-1814
4 Bde. Halle: Gebauer 1804-1806
8 Bände. Halle: Gebauer 1802-1809
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
3 Bde. Halle: Gebauer 1803-1806
8 Bände. Halle: Gebauer 1802-1809
13 Teile. Halle: Gebauer 11780-1792, 2-51794-1807
8 Bände. Halle: Gebauer 1802-1809
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
2 Teile. Halle: Gebauer 1803-1808
5 Teile. Halle: Gebauer 11782-1821, 21799-1808, 31809-1811
8 Bände. Halle: Gebauer 1802-1809
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
3 Teile. Halle: Gebauer 11782-1794, 21799-1808, 31809-1821
8 Bände. Halle: Gebauer 1802-1809
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
2 Bde. Halle: Gebauer 1810-1813
66 Teile. Halle: Gebauer 1744-1814
6 Teile. Halle: Gebauer 11787-1794, 21795-1799, 31810-1824
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
5 Teile. Halle: Gebauer 11782-1794, 21799-1808, 31809-1821
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
2 Bde. Halle: Gebauer 1810-1813
6 Teile. Halle: Gebauer 11787-1794, 21795-1799, 31810-1824
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
66 Teile. Halle: Gebauer 1744-1814
12 Bde. Halle: Gebauer 11805-1818, 21820-1821
6 Teile. Halle: Gebauer 11787-1794, 21795-1799, 31810-1824
12 Bde. Halle: Gebauer 1805-1818
4 Teile. Halle: Gebauer 1819-1822
