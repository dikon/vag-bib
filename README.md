# Verlagsbibliographie Gebauer 1734–1819 im JSON-Format

Ausgangspunkt für die hier in Form einer [JSON-Datei](./data/verlagsbibliographie.json) vorliegende Bibliographie ist das von Marcus Conrad und Manuel Schulz zusammengetragene Verzeichnis der von Johann Justinus Gebauer († 1772) und dessen Sohn Johann Jakob Gebauer († 1818) in der familieneigenen Verlagsdruckerei hergestellten Bände:

> Marcus Conrad und Manuel Schulz, Verlagsbibliographie Gebauer (1734–1819), in: _Merkur und Minerva. Der Hallesche Verlag Gebauer im Europa der Aufklärung_. Hrsg. von Daniel Fulda und Christine Haug. _Buchwissenschaftliche Beiträge_ 89. Wiesbaden: Harrassowitz, 2014, S. 262–458.

Als [PDF-Dokument](http://www.gebauer-schwetschke.halle.de/push.aspx?s=downloads/gs/home//Verlagsbibliographie/verlagsbibliographie_1734-1819.pdf) ist es über die Webseite zum [Erschließungsprojekt der Verlagsnachlässe von Gebauer und Schwetschke](http://www.gebauer-schwetschke.halle.de) (2010–2014) frei zugänglich. Auf Basis der dem PDF-Dokument zugrundeliegenden Word-Datei wurde zunächst eine TXT-Datei erstellt und die bibliographischen Daten davon ausgehend [JSON-basiert](./schema.txt) schematisiert.
